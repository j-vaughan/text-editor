target:	editor

editor:	main.o Makefile
	gcc main.o -o editor -I.

main.o:	src/main.c Makefile
	gcc -c src/main.c -I.